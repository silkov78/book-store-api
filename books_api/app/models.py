from sqlalchemy import Column, Integer, Float, String

from db_config import Base


class Book(Base):
    __tablename__ = "books"

    id = Column(Integer, primary_key=True)
    title = Column(String(80), nullable=False)
    description = Column(String)
    price = Column(Float, nullable=False)

    def __repr__(self):
        return f'Book {self.title}: {self.price} $'
