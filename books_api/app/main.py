from fastapi import FastAPI, Depends, HTTPException, Response, Request
from fastapi.responses import JSONResponse
from typing import Annotated, List
from sqlalchemy.orm import Session

from jose import jwt

from models import Book
from schema import BookSchema
from db_config import engine, SessionLocal, Base

app = FastAPI()
Base.metadata.create_all(bind=engine)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


db_dependency = Annotated[Session, Depends(get_db)]

# JWT-token validation
SECRET_KEY = "very-secret-secret-key"
ALGORITHM = "HS256"


# @app.middleware("http")
# async def jwt_validation_middleware(request: Request, call_next):
#     authorization_content = request.headers.get("Authorization", None)

#     if not authorization_content:
#         return JSONResponse(status_code=401, content={"message": "Access token isn't provided"})

#     try:
#         # to remove "AUTH_HEADER_TYPES" from authorization_content
#         token = authorization_content.split(' ')[1]
#         payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])

#     except (jwt.JWTError, IndexError):

#         return Response(status_code=401, content="Invalid access token")

#     response = await call_next(request)
#     return response


# CRUD operations
@app.get("/api/v1/books/")
async def get_book_list(db: db_dependency):
    return db.query(Book).all()


@app.post("/api/v1/books/")
async def create_book(book: BookSchema, db: db_dependency):
    db_book = Book(
        title=book.title,
        description=book.description,
        price=book.price
    )
    db.add(db_book)
    db.commit()
    return Response(content="Book is added!")


@app.delete("/api/v1/books/")
async def delete_all_books(db: db_dependency):
    db.query(Book).delete(synchronize_session=False)
    db.commit()
    return Response(status_code=204)


@app.get("/api/v1/books/{book_id}/")
async def get_book(book_id: int, db: db_dependency):
    result = db.query(Book).filter(Book.id == book_id).first()
    if not result:
        raise HTTPException(status_code=404, detail=f"Book {book_id} is not found!")
    return result


@app.delete("/api/v1/books/{book_id}/")
async def delete_book(book_id: int, db: db_dependency):
    deleted_book = db.query(Book).filter(Book.id == book_id)
    if not deleted_book:
        raise HTTPException(status_code=404, detail=f"Book {book_id} is not found!")
    else:
        deleted_book.delete(synchronize_session=False)
        db.commit()
    return Response(content="Book is deleted!")


@app.put("/api/v1/books/{book_id}/")
async def update_book(book_id: int, book: BookSchema, db: db_dependency):
    updated_book = db.query(Book).filter(Book.id == book_id).first()
    if not updated_book:
        raise HTTPException(status_code=404, detail=f"Book {book_id} is not found!")
    updated_book.title = book.title
    updated_book.description = book.description
    updated_book.price = book.price
    db.add(updated_book)
    db.commit()
    return Response(content="Book is updated!")
