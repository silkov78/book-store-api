from pydantic import BaseModel


class BookSchema(BaseModel):
    title: str
    description: str
    price: float
