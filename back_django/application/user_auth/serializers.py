from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.validators import UniqueValidator


class RegisterUserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(
        required=True,
        write_only=True
    )

    is_staff = serializers.BooleanField(
        default=False
    )

    email = serializers.EmailField(
        required=False
    )

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'is_staff', 'email')

    def create(self, validated_data):
        new_user = User(
            username=validated_data['username'],
            is_staff=validated_data['is_staff'],
            email=validated_data['email']
        )
        new_user.set_password(validated_data['password'])
        new_user.save()

        return new_user


class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'is_staff', 'email')
